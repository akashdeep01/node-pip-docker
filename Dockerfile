FROM ubuntu:18.04

RUN apt-get update && apt-get -y --force-yes install wget gettext-base python-pip npm gcc git python-setuptools python-cffi
RUN apt-get -y --force-yes install openjdk-8-jdk maven
